import 'package:business_manger/src/dataModel/Data.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class Order extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OrderState();
  }
}

class OrderState extends State<Order>  {

  List<Data> allData=[];
  @override
  void initState() {

     DatabaseReference ref=FirebaseDatabase.instance.reference();
     ref.child('Customers').once().then((DataSnapshot snap){
          var keys=snap.value.key;
          var data=snap.value;

          for(var key in keys){
            Data d=new Data(
           data[key]['Ahmed']
            );
            allData.add(d);
          }
          setState(() {
            print(allData.length);
          });
       });

  }
  final GlobalKey<FormState> formkey = new GlobalKey<FormState>();
  var selectedName;
  List<String> names = <String>['Esraa', 'Abdo', 'Mohamed','Eslam'];

  @override
  Widget build(context) {
    return Container(
      child: Form(
          child: Column(
        children: customerName()
        /* productName();
             productQuantity();
             productPrice();
             total();
             submitButon();
             Container(EdgeInsets.all(20));*/
        ,
      )),
    );
  }




  List<Widget> customerName() {
    return <Widget>[
      new Row(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Customer Name : ',
              style: TextStyle(
                color: Colors.blue,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
          new Padding(
            padding: EdgeInsets.all(20),
            child: new DropdownButton(
              items: names
                  .map((value) => DropdownMenuItem(
                        child: Text(
                          value,
                          style: TextStyle(color: Colors.brown,
                          backgroundColor: Colors.amber),
                        ),
                value: value,
                      )).toList(),
                onChanged: (selectedCustomerName) {
                setState(() {
                  selectedName = selectedCustomerName;
                });
              },
              key: formkey,
              value: selectedName,
              hint: Text('choose Customer Name',
                style: TextStyle(
                  color: Colors.deepPurple,
                ),
              ),
              isExpanded: false,
            ),
          ),
        ],
      ),
    ];
  }

  List<Widget> productName() {
    return <Widget>[
      new Row(
        children: <Widget>[
          new Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Name : ',
              style: TextStyle(
                color: Colors.blue,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
          new Padding(
            padding: EdgeInsets.all(20),
            child: new DropdownButton(
              items: names
                  .map((value) => DropdownMenuItem(
                child: Text(
                  value,
                  style: TextStyle(color: Colors.brown,
                      backgroundColor: Colors.amber),
                ),
                value: value,
              )).toList(),
              onChanged: (selectedCustomerName) {
                setState(() {
                  selectedName = selectedCustomerName;
                });
              },
              key: formkey,
              value: selectedName,
              hint: Text('choose Customer Name',
                style: TextStyle(
                  color: Colors.deepPurple,
                ),
              ),
              isExpanded: false,
            ),
          ),
        ],
      ),
    ];
  }


}
